"""add updated column on Film and add one to many relationship User to Film

Revision ID: 19c4347850f6
Revises: 4c9306a5802c
Create Date: 2013-11-04 19:17:17.429588

"""

# revision identifiers, used by Alembic.
revision = '19c4347850f6'
down_revision = '4c9306a5802c'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('film', sa.Column('updated', sa.DateTime(timezone=True)))
    op.add_column('film', sa.Column('user_id', sa.Integer, sa.ForeignKey('user.id')))


def downgrade():
    op.drop_column('film', 'updated')
    op.drop_column('film', 'user_id')
