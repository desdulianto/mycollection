"""add many to many relationship user and film

Revision ID: 59b98a104c11
Revises: 19c4347850f6
Create Date: 2013-11-04 21:11:00.347340

"""

# revision identifiers, used by Alembic.
revision = '59b98a104c11'
down_revision = '19c4347850f6'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('user_film',
        sa.Column('user_id', sa.Integer, sa.ForeignKey('user.id')),
        sa.Column('film_id', sa.Integer, sa.ForeignKey('film.id')))


def downgrade():
    op.drop_table('user_film')
