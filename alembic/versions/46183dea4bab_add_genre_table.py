"""add genre table

Revision ID: 46183dea4bab
Revises: 4fd2f87b8277
Create Date: 2013-11-03 13:28:48.652614

"""

# revision identifiers, used by Alembic.
revision = '46183dea4bab'
down_revision = '4fd2f87b8277'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('genre',
        sa.Column('id'   , sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('genre', sa.String(255), index=True, unique=True))

def downgrade():
    op.drop_table('genre')
