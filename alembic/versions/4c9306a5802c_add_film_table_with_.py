"""add film table with many to many relationship to genre

Revision ID: 4c9306a5802c
Revises: 46183dea4bab
Create Date: 2013-11-03 13:38:49.765041

"""

# revision identifiers, used by Alembic.
revision = '4c9306a5802c'
down_revision = '46183dea4bab'

from alembic import op
import sqlalchemy as sa


def upgrade():
    # film table
    op.create_table('film',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('title', sa.String(255), index=True),
        sa.Column('year', sa.String(4)))

    # film many to many to genre
    op.create_table('film_genre',
        sa.Column('film_id', sa.Integer, sa.ForeignKey('film.id')),
        sa.Column('genre_id', sa.Integer, sa.ForeignKey('genre.id')))


def downgrade():
    op.drop_table('film_genre')
    op.drop_table('film')
