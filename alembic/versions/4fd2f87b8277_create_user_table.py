"""create user table

Revision ID: 4fd2f87b8277
Revises: None
Create Date: 2013-11-03 13:09:54.727993

"""

# revision identifiers, used by Alembic.
revision = '4fd2f87b8277'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'user',
        sa.Column('id'      , sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('nickname', sa.String(255), index=True),
        sa.Column('fullname', sa.String(255), index=True),
        sa.Column('email'   , sa.String(255), index=True),
        sa.Column('openid'  , sa.String(255)),
        sa.Column('active'  , sa.Boolean),
        sa.Column('role'    , sa.Enum('admin', 'user', name='role_types')))



def downgrade():
    op.drop_table('user')
