from flask.ext.wtf import Form
from wtforms import TextField, SelectMultipleField
from wtforms.validators import DataRequired, ValidationError

import models


class GenreForm(Form):
    genre = TextField('Genre', validators=[DataRequired()])

    def validate_genre(form, field):
        genre = field.data
        if len(models.Genre.query.filter_by(genre=genre).all()) > 0:
            raise ValidationError('Genre %s already exist' % genre)


class FilmForm(Form):
    title = TextField('Title', validators=[DataRequired()])
    year = TextField('Year')
    genres = SelectMultipleField('Genres')
    def __init__(self, formdata=None, obj=None, prefix='', LOCALES=None, 
            **kwargs):
        super(FilmForm, self).__init__(formdata, obj, prefix, LOCALES, 
                **kwargs)
        self.genres.choices = [(unicode(g.id), g.genre) 
                for g in models.Genre.query.order_by(
                    models.Genre.genre).all()]

    def validate_year(form, field):
        if field.data == '':
            return
        try:
            year = int(field.data)
            if not (1900 < year < 2100):
                raise ValidationError('Year must be between 1900 - 2100')
        except:
            raise ValidationError('Year must be number')


class FilmSearch(Form):
    q = TextField('Film Title', validators=[DataRequired()])


class UserSearch(Form):
    q = TextField('User Name', validators=[DataRequired()])
