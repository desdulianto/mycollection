from flask import render_template, g, request, redirect, abort, url_for, \
        jsonify
from flask.ext.login import login_user, current_user, logout_user, \
        login_required
from flask.ext.paginate import Pagination
from werkzeug.exceptions import InternalServerError, NotFound, HTTPException
import requests
import operator
from sqlalchemy import or_
from sqlalchemy.sql import func

from app import app, openid, login_manager, db
import models
import forms


@app.before_request
def before_request():
    g.user = current_user
    g.menus = [dict(title='Index', url=url_for('index')),
               dict(title='Users', url=url_for('user_list'))]


@login_manager.user_loader
def load_user(userid):
    return models.User.query.get(userid)


@openid.after_login
def after_openid_login(resp):
    user = models.User.query.filter_by(email=resp.email).first()

    # create new user
    if user is None:
        user = models.User(nickname=resp.nickname, fullname=resp.fullname,
                email=resp.email, openid=resp.identity_url, active=True, 
                role='user')
        try:
            db.session.add(user)
            db.session.commit()
        except:
            raise InternalServerError('Database error')
    login_user(user)
    if request.args.get('next') == url_for('login'):
        reques.args['next'] = url_for('index')
    return redirect(request.args.get('next') or url_for('index'))


@app.route('/login', methods=['GET', 'POST'], endpoint='login')
@openid.loginhandler
def login():
    if g.user.is_authenticated():
        return redirect(openid.get_next_url())
    if request.method == 'POST':
        oid = request.form.get('openid')
        if oid:
            return openid.try_login(oid, ask_for=['email', 
                'fullname', 'nickname'])
    return render_template('login.html', next=openid.get_next_url(),
            error=openid.fetch_error(), 
            providers=app.config['OPENID_PROVIDERS'])


@app.route('/logout', endpoint='logout')
def logout():
    logout_user()
    return render_template('logout.html')


# film views
# index
@app.route('/', methods=['GET', 'POST'], endpoint='index')
@login_required
def index():
    form = forms.FilmSearch()
    per_page = 20
    try:
        page = int(request.args.get('page', 1))
    except:
        page = 1

    # query movies from database
    query = models.Film.query.order_by(models.Film.title)

    total = query.count()

    films = query.limit(per_page).offset((page-1)*per_page).all()

    movies = []
    for film in films:
        movie = rotten.movie_detail(film.id)
        f = filter(lambda x: x.user == g.user, film.items)
        if len(f) > 0:
            movie['haveit'] = f[0].haveit
            movie['wantit'] = f[0].wantit
            movie['download'] = f[0].download
        movies.append(movie)

    pagination = Pagination(page=page, total=total, search=False, per_page=20,
        record_name='movies', bs_version=3)

    return render_template('index.html', form=form, movies=movies,
            pagination=pagination)


# search film
@app.route('/search', methods=['GET'], endpoint='movie_search')
@login_required
def movie_search():
    q = request.args.get('q')
    try:
        page = int(request.args.get('page', 1))
    except:
        page = 1

    data = rotten.movie_search(q, page=page)
    if data is not None:
        total = int(data['total'])
        movies = data['movies']
        template = data['link_template']
        links = data['links']
    else:
        abort(400)

    # cari di database untuk film yang dibaca dari rotten tomatoes
    # i think this process can be further optimized
    userfilms = g.user.films.filter(
            models.UserFilms.film_id.in_([x['id'] for x in movies])).all()
    userfilmsid = [unicode(u.film.id) for u in userfilms]
    updatemovies = filter(lambda x: x['id'] in userfilmsid, movies)

    for f in userfilms:
        m = filter(lambda x: x['id'] == unicode(f.film.id), updatemovies)
        m[0]['haveit'] = f.haveit
        m[0]['wantit'] = f.wantit
        m[0]['download'] = f.download

    movies = sorted(movies, key=operator.itemgetter('title'))

    pagination = Pagination(page=page, total=total, search=False, per_page=20,
        record_name='movies', bs_version=3)

    # tampilkan hasil pencarian
    return render_template('index.html', form=forms.FilmSearch(), 
            movies=movies, pagination=pagination)


# movie detail
@app.route('/movie/<int:movie_id>', methods=['GET', 'POST'], 
        endpoint='movie_detail')
@login_required
def movie_detail(movie_id):
    movie = rotten.movie_detail(movie_id)
    if movie is not None:
        films = models.UserFilms.query.filter_by(film_id=movie_id)
        movie['have'] = [u.user for u in films.filter_by(haveit=True).all()]
        movie['want'] = [u.user for u in films.filter_by(wantit=True).all()]
        movie['download'] = [u.user for u in films.filter_by(download=True).all()]

        movie['have'] = sorted(movie['have'],
            key=operator.attrgetter('fullname'))
        movie['want'] = sorted(movie['want'],
            key=operator.attrgetter('fullname'))
        movie['download'] = sorted(movie['download'],
            key=operator.attrgetter('fullname'))

        movie['haveit'] = (True if len(filter(lambda x: x == g.user,
            movie['have'])) > 0 else False)
        movie['wantit'] = (True if len(filter(lambda x: x == g.user,
            movie['want'])) > 0 else False)
        movie['downloadit'] = (True if len(filter(lambda x: x == g.user,
            movie['download'])) > 0 else False)
    else:
        abort(404)
    return render_template('movie_detail.html', movie=movie)



def toggle(user, movie_id, field):
    # check movie_id to rotten tomatoes first
    m = rotten.movie_detail(movie_id)
    if m is None:
        raise NotFound('Movie not found')

    fields = ('wantit', 'haveit', 'download')
    movie = models.Film.query.get(movie_id)
    if movie is None:
        movie = models.Film(id=movie_id, title=m['title'])

    film = user.films.filter_by(film_id=movie_id).first()
    if film is None:
        film = models.UserFilms()
        setattr(film, field, True)
        film.film = movie
        g.user.films.append(film)
    else:
        yes = not getattr(film, field)
        setattr(film, field, not getattr(film, field))
        # if is yes, set other field to false
        if yes:
            for f in filter(lambda x: x != field, fields):
                setattr(film, f, False)
    
    try:
        db.session.add(movie)
        db.session.add(film)
        db.session.add(g.user)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        raise InternalServerError('Database Error!')
    return film


def toggle_helper(movie_id, tag):
    try:
        film = toggle(g.user, movie_id, tag)
        status = (dict(status='ok', have=film.haveit, want=film.wantit,
            download=film.download), 200)
    except HTTPException as e:
        status = (dict(status=e.get_description()), 500)
    return jsonify(data=status[0]), status[1]



@app.route('/haveit/<int:movie_id>', methods=['POST'], endpoint='haveit')
@login_required
def haveit(movie_id):
    return toggle_helper(movie_id, 'haveit')


@app.route('/wantit/<int:movie_id>', methods=['POST'], endpoint='wantit')
@login_required
def wantit(movie_id):
    return toggle_helper(movie_id, 'wantit')


@app.route('/download/<int:movie_id>', methods=['POST'], endpoint='download')
@login_required
def download(movie_id):
    return toggle_helper(movie_id, 'download')


# user movie list
@app.route('/user/<int:user_id>', methods=['GET'], endpoint='user_movie_list')
@login_required 
def user_movie_list(user_id):
    per_page = 20
    try:
        page = int(request.args.get('page', 1))
    except:
        page = 1

    user = models.User.query.get_or_404(user_id)
    query = user.films.filter(or_(models.UserFilms.haveit==True,
        models.UserFilms.wantit==True, models.UserFilms.download==True))
    total = query.count()
    items = (query.join(models.Film).order_by(models.Film.title).
             limit(per_page).offset((page-1)*per_page).all())
    movies = []
    for item in items:
        movie = rotten.movie_detail(item.film.id)
        if movie is None:
            continue
        movie['haveit'] = item.haveit
        movie['wantit'] = item.wantit
        movie['download'] = item.download
        movies.append(movie)
    # sort movies
    movies = sorted(movies, key=operator.itemgetter('title'))

    pagination = Pagination(page=page, total=total, search=False, per_page=20,
        record_name='movies', bs_version=3)

    return render_template('index.html',  form=forms.FilmSearch(),
            movies=movies, caption='{name}\'s Movie List'.format(name=user.fullname),
            pagination=pagination)


# user views
@app.route('/user', endpoint='user_list')
def user_list():
    form = forms.UserSearch()
    q = request.args.get('q', None)
    per_page = 20
    try:
        page = int(request.args.get('page', 1))
    except ValueError:
        page = 1

    users = models.User.query.order_by(models.User.fullname)
    if q:
        users = users.filter(or_(func.lower(models.User.nickname).like('%' +
            q.lower() + '%'), func.lower(models.User.fullname).like('%' +
                q.lower() + '%')))
    pagination = Pagination(page=page, total=users.count(), search=False,
        per_page=per_page, record_name='users', bs_version=3)
    return render_template('user_list.html',
            users=users.limit(per_page).offset((page-1)*per_page).all(),
        pagination=pagination, form=form)


# rotten api
class Rotten(object):
    def __init__(self, apikey, baseurl):
        self.apikey = apikey
        self.baseurl = baseurl

    def movie_detail(self, movie_id):
        url = self.baseurl + '/movies/{movie_id}.json'.format(
                movie_id=movie_id)
        params = dict(apikey=self.apikey)
        resp = requests.get(url, params=params)

        if resp.status_code == 200:
            movie = resp.json()
        else:
            movie = None
        return movie

    def movie_search(self, q, page=1):
        url = self.baseurl + '/movies.json'
        params = dict(q=q, page_limit=20, page=page, apikey=self.apikey)
        resp =  requests.get(url, params=params)
        if resp.status_code == 200:
            data = resp.json()
        else:
            data = None
        return data
        #total = data['total']
        #movies = data['movies']
        #template = data['link_template']
        #links = data['links']


rotten = Rotten(apikey=app.config['ROTTEN_TOMATOES_API_KEY'],
            baseurl=app.config['ROTTEN_TOMATOES_BASE_URL'])
