from datetime import datetime
from dateutil import tz
from flask.ext.login import UserMixin
from app import db


utcnow = lambda: datetime.now(tz=tz.tzutc())


class UserFilms(db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    film_id = db.Column(db.Integer, db.ForeignKey('film.id'), primary_key=True)
    haveit = db.Column(db.Boolean, default=False)
    wantit  = db.Column(db.Boolean, default=False)
    download= db.Column(db.Boolean, default=False)
    film    = db.relationship('Film', backref='items', order_by='Film.title')
    updated = db.Column(db.DateTime(timezone=True), default=utcnow, onupdate=utcnow)


class User(db.Model, UserMixin):
    id           = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nickname     = db.Column(db.String(255), index=True)
    fullname     = db.Column(db.String(255), index=True)
    email        = db.Column(db.String(255), index=True)
    openid       = db.Column(db.String(255))
    active       = db.Column(db.Boolean)
    role         = db.Column(db.Enum('admin', 'user', name='role_types'))
    films        = db.relationship('UserFilms', backref='user', lazy='dynamic')

    def is_active(self):
        return self.active

    def has_film(self, film):
        return film in self.films

    def __repr__(self):
        return '<User %s>' % (self.fullname)


class Film(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(1024), index=True)
    updated = db.Column(db.DateTime(timezone=True), default=utcnow, onupdate=utcnow)

    def __repr__(self):
        return '<Film %s>' % (self.title)
