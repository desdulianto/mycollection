from flask import Flask, g, session
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager, current_user
from flask.ext.openid import OpenID


app = Flask(__name__)

app.config.from_object('config')

# init db
db = SQLAlchemy(app)

# init login
login_manager = LoginManager(app)
login_manager.login_view = 'login'

# init openid
openid = OpenID(app)

# views
from app import views
