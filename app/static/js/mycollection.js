function toggle_film(config) {
    var preds = new Array(config['haveit-pred'],
                          config['wantit-pred'],
                          config['download-pred']);

    function toggle(obj) {
            var elem = $(this);
            var id = elem.attr("id").split("-")[1];

            url = $(obj.target).attr('data-url');

            $.post(url, function() {
                    })
            .done(function(data) {
                    json = data['data'];
                    for (i in preds) {
                        p = preds[i];
                        e = $("#" + p + "-" + id);
                        yestext = config[i+'-yes-text'] ?
                                  config[i+'-yes-text'] :
                                  config['yes-text'];
                        notext = config[i+'no-text'] ?
                                 config[i+'no-text'] :
                                 config['no-text'];

                        if (json[p]) {
                            e.text(yestext);
                        } else {
                            e.text(notext);
                        }
                    }
                    })
            .fail(function(data) {
                    console.log(JSON.stringify(data));
                    });
    }
    $("." + config['haveit-class']).click(toggle);
    $("." + config['wantit-class']).click(toggle);
    $("." + config['download-class']).click(toggle);
}
